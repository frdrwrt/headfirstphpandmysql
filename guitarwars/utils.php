<?php
/**
 * Created by PhpStorm.
 * User: practicewild
 * Date: 19.08.18
 * Time: 16:00
 */

require_once('appvars.php');

function is_valid_screenshot($screenshot)
{
    if ($screenshot['size'] <= 0 || $screenshot['size'] > GW_MAX_FILE_SIZE) {
        return false;
    }
    if ($screenshot['type'] != 'image/gif' &&
        $screenshot['type'] != 'image/pjpeg' &&
        $screenshot['type'] != 'image/png' &&
        $screenshot['type'] != 'image/jpeg') {
        return false;
    }
    if ($screenshot['error'] != 0) {
        return false;
    }
    return true;
}